package sine;

import java.util.function.Function;

class ChirpFunction implements Function<Double, Double> {

    private double highFrequency;
    private double lowFrequency;
    private double amplitude;
    private double samples;

    public static class ChirpFunctionBuilder {
        private double highFrequency;
        private double lowFrequency;
        private double amplitude;
        private double samples;

        public ChirpFunctionBuilder setHighFrequency(double highFrequency) {
            this.highFrequency = highFrequency;
            return this;
        }

        public ChirpFunctionBuilder setLowFrequency(double lowFrequency) {
            this.lowFrequency = lowFrequency;
            return this;
        }

        public ChirpFunctionBuilder setAmplitude(double amplitude) {
            this.amplitude = amplitude;
            return this;
        }

        public ChirpFunctionBuilder setSamples(double samples) {
            this.samples = samples;
            return this;
        }

        public ChirpFunction create() {
            ChirpFunction r = new ChirpFunction();
            if( highFrequency < lowFrequency) throw new IllegalArgumentException("HighFrequency should be higher than lowFrequency");
            if(lowFrequency < 0 ) throw new IllegalArgumentException("low frequency msut be grater than 0");
            if(samples < 1 ) throw new IllegalArgumentException("Samples should be grater then 1");
            r.setHighFrequency(highFrequency);
            r.setLowFrequency(lowFrequency);
            r.setAmplitude(amplitude);
            r.setSamples(samples);

            return r;
        }
    }


    public double getHighFrequency() {
        return highFrequency;
    }

    public void setHighFrequency(double highFrequency) {
        this.highFrequency = highFrequency;
    }

    public double getLowFrequency() {
        return lowFrequency;
    }

    public void setLowFrequency(double lowFrequency) {
        this.lowFrequency = lowFrequency;
    }

    public double getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(double amplitude) {
        this.amplitude = amplitude;
    }

    public void setSamples(double samples) { this.samples = samples; }

    @Override
    public Double apply(Double aDouble) {
        return Math.sin(0);
    }
}


package sine;

import javax.swing.*;
import java.awt.*;


public class SinusApp extends JPanel {
    private SineDraw sines = new SineDraw();

    private JSlider adjustCycles = new JSlider(1, 30, 5);

    private SinusApp() {
        super(new BorderLayout());
        add(BorderLayout.CENTER, sines);
        adjustCycles.addChangeListener(e -> sines.setCycles(((JSlider) e.getSource()).getValue()));
        add(BorderLayout.SOUTH, adjustCycles);
    }

    public static void main(String[] args) {
        JPanel p = new SinusApp();

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(p);
        frame.setSize(500, 500);

        frame.setVisible(true);
    }
}

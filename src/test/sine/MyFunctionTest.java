package sine;

import org.junit.Test;

public class MyFunctionTest {

    @Test(expected = IllegalArgumentException.class)
    public void checkHighNotZero(){

        new ChirpFunction.ChirpFunctionBuilder().setHighFrequency(-1).create();

    }


    @Test(expected = IllegalArgumentException.class)
    public void checkHighGraterThanLower(){
        new ChirpFunction.ChirpFunctionBuilder().setLowFrequency(-1).create();
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkSamplesnotOne(){
        new ChirpFunction.ChirpFunctionBuilder().setSamples(-1).create();
    }

}